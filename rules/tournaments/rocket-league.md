# Rocket League

## General Provisions

These rules inherit all rules from the main rulebook.

These rules apply to all participants of the Rocket League tournament (hereinafter "RL" or "Game").

The rules may be available in several languages. If the individual versions differ, the English version is considered the source of the truth.

## Tournament Information

### Tournament Platform

The official tournament platform for this tournament is Battlefy.
Please make sure that each member of your team has an account.

### Tournament Format

Depends on the amount of players. Will be decided during the captain briefing.

### Schedule

First appointment for the team representative will be the briefing on Saturday at 10:00. The Tournament itself will start one hour later at 11:00.
All players must be at the location at the time of the briefing.

### Game Settings

| Setting      | Value     |
|--------------|-----------|
| Game Mode    | Soccar    |
| Team Size    | 3v3       |
| Region       | Europe    |
| Match Length | 5 minutes |

### Map Pool

* DFH Stadium
* Mannfield
* Champions Field
* Neo Tokyo
* Urban Central,
* Beckwick Park
* Utopia Coliseum
* Aquadome
* Forbidden template
 
Both night and day version are allowed. The Stormy and Snowy are **not** allowed.

### Map Picking

The 1st map is of each round is DFH Stadium.
The losing team chooses the map for next round.
